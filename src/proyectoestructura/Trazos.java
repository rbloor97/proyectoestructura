/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoestructura;

import java.util.ArrayList;

/**
 *
 * @author Edmundoleonidas
 */
public class Trazos {
    private ArrayList<Double> x;
    private ArrayList<Double> y;
    
    public Trazos(){
        x = new ArrayList<>();
        y = new ArrayList<>();
    }

    public ArrayList<Double> getX() {
        return x;
    }

    public void setX(ArrayList<Double> x) {
        this.x = x;
    }

    public ArrayList<Double> getY() {
        return y;
    }

    public void setY(ArrayList<Double> y) {
        this.y = y;
    }
    
}
