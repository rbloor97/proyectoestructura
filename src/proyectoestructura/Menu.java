/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoestructura;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import org.json.simple.parser.ParseException;

public class Menu {
    
    private BorderPane root;
    private Button inicio, vertop, salir;
    private Pane panel;
    private VBox box;
    private Button personaje;
    
    Menu() {
        
        
        organizarpanel();
    }
    
    public void organizarpanel(){
        root = new BorderPane();
        box = new VBox();
        personaje = new Button();
        
        inicio = new Button();
        vertop = new Button();
        salir = new Button();
        
      

        inicio.setStyle("-fx-background-color: transparent; ");
        ImageView image = new ImageView("imagenes/jugar.png");
	image.setFitWidth(200);
	image.setFitHeight(125);
	inicio.setGraphic(image);
        
        inicio.setOnMouseEntered(new EventHandler<MouseEvent>() {
        @Override public void handle(MouseEvent e) {
            inicio.setScaleX(1.2);
            inicio.setScaleY(1.2);
            }
        });
 
        inicio.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override public void handle(MouseEvent e) {
                inicio.setScaleX(1);
                inicio.setScaleY(1);
            }
        });
        
        
        
     inicio.setOnAction(e -> {try {
         ProyectoEstructura.cambiarVentana(root, new PaneOrganizer().getRoot());
            } catch (IOException ex) {
                Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
            }});
     
     
     salir.setOnAction(el -> {try {
         ProyectoEstructura.cambiarVentana(root, new Dibujos().getRoot());
         } catch (IOException ex) {
             Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
         } catch (ParseException ex) {
             Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
         }

});
//         inicio.setOnAction(new EventHandler<ActionEvent>(){
//            @Override
//            public void handle(ActionEvent t) {
//               
//                 
//                 
//                try {
//                    ProyectoEstructura.Verdibujos();
//                } catch (IOException ex) {
//                    Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
//                } catch (ParseException ex) {
//                    Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
//                }
//                 
//                 
//            }
//        });
        
        
        
        vertop.setStyle("-fx-background-color: transparent; ");
        ImageView image1 = new ImageView("imagenes/jugadores.png");
	image1.setFitWidth(225);
	image1.setFitHeight(150);
	vertop.setGraphic(image1);
        
        vertop.setOnMouseEntered(new EventHandler<MouseEvent>() {
        @Override public void handle(MouseEvent e) {
            vertop.setScaleX(1.2);
            vertop.setScaleY(1.2);
            }
        });
 
        vertop.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override public void handle(MouseEvent e) {
                vertop.setScaleX(1);
                vertop.setScaleY(1);
            }
        });
//        vertop.setOnAction(new EventHandler<ActionEvent>(){
//            public void handle(ActionEvent t) {
//               
//               
//
//                }
//        });
        
        
        salir.setStyle("-fx-background-color: transparent; ");
        ImageView image2 = new ImageView("imagenes/salir.png");
	image2.setFitWidth(175);
	image2.setFitHeight(125);
	salir.setGraphic(image2);
        
           salir.setOnMouseEntered(new EventHandler<MouseEvent>() {
        @Override public void handle(MouseEvent e) {
            salir.setScaleX(1.2);
            salir.setScaleY(1.2);
            }
        });
 
        salir.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override public void handle(MouseEvent e) {
                salir.setScaleX(1);
                salir.setScaleY(1);
            }
        });
      /*  salir.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(ActionEvent t) {
               
               

                }
        });*/
        
        box.getChildren().addAll(inicio,vertop,salir);
        
        
        
        root.setStyle("-fx-background-image: url('/imagenes/fondo.jpg'); "
                + "-fx-background-position: center center; "
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size:" + Constantes.ANCHO + " " + Constantes.ALTO + ";");

        box.setAlignment(Pos.CENTER);
        box.setSpacing(75);
        root.setCenter(box);               
        
       
      
        
       
      
        
        
     
        
    
        
         
     
        
        
        
   
    }
  
  
   
    BorderPane getRoot() {
        return root;
    }

    
}
