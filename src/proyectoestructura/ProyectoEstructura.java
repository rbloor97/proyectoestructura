/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoestructura;

import java.io.FileNotFoundException;
import java.io.IOException;
import javafx.application.Application;
import javafx.stage.Stage;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import org.json.simple.parser.ParseException;

/**
 *
 * @author adan
 */
public class ProyectoEstructura extends Application{
    private static Scene escena;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // TODO code application logic here
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle( "Proyecto Estructura" );
        escena= new Scene(new Menu().getRoot(),Constantes.ANCHO,Constantes.ALTO);
        primaryStage.setScene(escena);
        primaryStage.show();//To change body of generated methods, choose Tools | Templates.
    }
    
     public static void Verdibujos() throws IOException, FileNotFoundException, ParseException{
        Dibujos dibujos = new Dibujos();
 
        escena.setRoot(dibujos.getRoot());
        escena.onKeyPressedProperty().bind(dibujos.getRoot().onKeyPressedProperty());
    }
     
     public static void Jugar() throws IOException, FileNotFoundException, ParseException{
        PaneOrganizer pane = new PaneOrganizer();
 
        escena.setRoot(pane.getRoot());
        escena.onKeyPressedProperty().bind(pane.getRoot().onKeyPressedProperty());
    }
     
     public static void Vertop() throws IOException, FileNotFoundException, ParseException{
        PaneOrganizer pane = new PaneOrganizer();
 
        escena.setRoot(pane.getRoot());
        escena.onKeyPressedProperty().bind(pane.getRoot().onKeyPressedProperty());
    }
     public static void cambiarVentana(Pane root, Pane root2 ){
        Scene scn = root.getScene();
         
         Stage stage = (Stage) scn.getWindow();
         
         Scene scn2 = new Scene( root2,Constantes.ANCHO, Constantes.ALTO );
         stage.setScene(scn2);
    }
    
}
