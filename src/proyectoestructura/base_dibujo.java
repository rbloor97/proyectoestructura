/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoestructura;

import java.util.ArrayList;

/**
 *
 * @author adan
 */
public class base_dibujo {
 public String word;
 public String countrycode;
 public ArrayList<ArrayList> trazos;

    public base_dibujo(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    @Override
    public String toString() {
        return "base_dibujo{" + "word=" + word + '}';
    }
 
 
}
