/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoestructura;



/**
 *
 * @author Kleberth
 * @param <E>
 */
public class CircularDoubleLinkedList<E> implements List<E> {
    private Nodo<E> first, last;
    private int efectivo;

    public CircularDoubleLinkedList(){
        this.first = this.last = null;
        this.efectivo = 0;
    }

   
    public boolean isEmpty() {
        return this.first==null && this.last==null;
    }

   
    public int size() {
        return efectivo;
    }

   
    public boolean addFirst(E element) {
        Nodo<E> nodo = new Nodo<>(element);
        if(element==null)
            return false;
        else if (this.isEmpty()) {
            this.first = nodo;
            //this.last.setNext(this.first);
            this.first.setPrevious(nodo);
        } else{
            Nodo<E> temp = new Nodo<>(element);
            nodo.setNext(this.first);
            nodo.setPrevious(temp);
            this.first.setPrevious(nodo);
            this.first = nodo;
            //this.last.setNext(nodo);
            //this.first.setPrevious(nodo);
            //nodo.setNext(this.first);
            //nodo.setPrevious(this.last);
            //this.first = nodo;
        }
        this.efectivo ++;
        return true;
    }
    
     public Nodo<E> getNodo() {
        
        if(this.isEmpty())
            return null;
        return this.first;
    }

   
    public boolean addLast(E element) {
        Nodo<E> nodo = new Nodo<>(element);
        if(element==null)
            return false;
        else if (this.isEmpty()) {
            this.first = nodo;
           // this.last.setNext(this.first);
            //this.first.setPrevious(this.last);
        } else {
            if (this.efectivo == 1) {
                nodo.setNext(first);
                nodo.setPrevious(first);
                first.setPrevious(nodo);
                first.setNext(nodo);
            } else {
                Nodo<E> temp = first.getPrevious();                
                temp.setNext(nodo);
                this.first.setPrevious(nodo);
                nodo.setNext(first);
                nodo.setPrevious(temp);
            }
        }
        this.efectivo ++;
        return true;
    }

    @Override
    public E getFirst() {
        if(this.isEmpty())
            return null;
        return this.first.getData();
    }

    @Override
    public E getLast() {
        if(this.isEmpty())
            return null;
        return this.last.getData();
    }

    @Override
    public boolean removeFirst() {
        if(this.isEmpty())
            return false;
        else if(this.first==this.last)
                this.first = this.last = null;
        else{
            Nodo<E> nodo = this.first;
            this.first = this.first.getNext();
            this.last.setNext(this.first);
            this.first.setPrevious(this.last);
            nodo.setNext(null);
            nodo.setPrevious(null);
        }
        this.efectivo --;
        return true;
    }

    @Override
    public boolean removeLast() {
        if(this.isEmpty())
            return false;
        else if(this.first==this.last)
                this.first = this.last = null;
        else{
            Nodo<E> nodo = this.last.getPrevious();
            nodo.setNext(this.first);
            this.first.setPrevious(nodo);
            this.last.setNext(null);
            this.first.setPrevious(null);
            this.last = nodo;
        }
        this.efectivo --;
        return true;
    }

    @Override
    public boolean contains(E element) {
        Nodo<E> p = this.first;
        Nodo<E> q = this.last;
        do{
            if (p.getData().equals(element) || q.getData().equals(element))
                return true;
            else{
                p = p.getNext();
                q = q.getPrevious();
            }
            
        }while(p!=this.first);  
        return false;
    }

    @Override
    public E get(int index) {
        if(index>=0 && index<efectivo){
            if(index==0)
                return this.first.getData();
            else if(index==efectivo-1)
                return this.last.getData();
            else{
                Nodo nodo = this.first;
                for (int i=0; i<index; i++) {
                    nodo = nodo.getNext();
                }
                return (E)nodo.getData();
            }
        }
        return null;
    }

    @Override
    public List<E> slicing(int inicio, int fin) {
        List<E> newLinked = new CircularDoubleLinkedList();
        if(inicio>=0 && inicio<=fin && fin<=this.efectivo-1){
            for(int i=inicio; i<=fin; i++){
                newLinked.addLast(this.get(i));
            }
            return newLinked;
            }
        return null;
    }

    @Override
    public boolean remove(int index) {
        if(this.isEmpty())
            return false;
        else if(this.first==this.last)
                this.first = this.last = null;
        else if(index==0)
            this.removeFirst();
        else if(index>=efectivo-1)
            this.removeLast();
        else{
            Nodo<E> p = this.first;
            for(int i=0; i<=index; i++){
                if(i==index){
                    p.getPrevious().setNext(p.getNext());
                    p.getNext().setPrevious(p.getPrevious());
                    p.setNext(null);
                    p.setPrevious(null);
                }
                p = p.getNext();
            }
            this.efectivo --;
            return true;
        }
        return false;
    }

    @Override
    public E set(int index, E element) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean add(int index, E element) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int indexOf(E element) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public String toString(){
        String str = "[";
        Nodo<E> p = this.first;
        do{
            str += p.getData() + ",";
            p = p.getNext();
        }while(p!=this.first);
        str = str.substring(0, str.length()-1)+"]";
        return str;
    }
}
