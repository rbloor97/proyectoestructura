/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoestructura;

import java.util.ArrayList;

/**
 *
 * @author adan
 */
public class Dibujo {
    private String word, countryCode,timestamp,key_id;
    private Boolean recognized;
    private ArrayList<Trazos> drawing;
    
    public Dibujo(String word,String countryCode,String timestamp, Boolean recognized, String key_id, ArrayList<Trazos> drawing){
        this.word=word;
        this.countryCode=countryCode;
        this.recognized=recognized;
        this.key_id=key_id;
        this.drawing=drawing;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Boolean getRecognized() {
        return recognized;
    }

    public void setRecognized(Boolean recognized) {
        this.recognized = recognized;
    }

    public String getKey_id() {
        return key_id;
    }

    public void setKey_id(String key_id) {
        this.key_id = key_id;
    }

    public ArrayList<Trazos> getDrawing() {
        return drawing;
    }

    public void setDrawing(ArrayList<Trazos> drawing) {
        this.drawing = drawing;
    }
    
    
}
