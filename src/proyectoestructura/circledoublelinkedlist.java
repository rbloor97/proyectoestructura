/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoestructura;

import java.util.LinkedList;


/**
 *
 * @author adan
 */
public class circledoublelinkedlist<Object> extends LinkedList<Object>{
    
    public NodoDoble start;
    

    public circledoublelinkedlist() {
        this.start=null;
    }


    public circledoublelinkedlist(NodoDoble start) {
        this.start = start;
        this.start.setPrevious(this.start);
        this.start.setNext(this.start);
    }
    
    public void add_element(Object obj){
        
        if(this.start==null){
            System.out.println("add_element_null");
            this.start = new NodoDoble(obj);
            this.start.setNext(this.start);
            this.start.setPrevious(this.start);
        }else{
            System.out.println("add_element_NONULL");
            NodoDoble temp = this.start.getPrevious();
            NodoDoble Nuevo = new NodoDoble(obj);
            Nuevo.setNext(this.start);
            Nuevo.setPrevious(temp);
            temp.setNext(Nuevo);
            this.start.setPrevious(Nuevo);
        }
    }

    public NodoDoble getStart() {
        return start;
    }



    public void setStart(NodoDoble start) {
        this.start = start;
    }

    
    
    
    
    
}
