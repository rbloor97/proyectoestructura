/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoestructura;

import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


/**
 *
 * @author Edmundoleonidas
 */
public class PaneOrganizer {
    private BorderPane root;
    private Button salir, verDibujos, siguienteJugador;
    private ComboBox<String> comboDibujos;
    private VBox panelInferior, panelSuperior, panelDerecho;
    private TextArea detalles;
    private Dibujo dibujoActual;
    /**
     *Lista con todos los dibujos.
     */
    public static CircularDoubleLinkedList listadoble;
    private static final String RUTA = "src/dataset_dibujos.ndjson";
    private static final String FONDO = "src/imagenes/fondoPO.jpg";
    public static String key_id;
    
    public PaneOrganizer() throws IOException, FileNotFoundException, ParseException{
        organizador();
    }
    
    public void organizador() throws IOException, FileNotFoundException, ParseException{
        listadoble = new CircularDoubleLinkedList();
        root = new BorderPane();
        salir = new Button("Salir");
        verDibujos = new Button("Ver Dibujos");
        siguienteJugador = new Button("Siguiente Jugador");
        comboDibujos = new ComboBox<>();
        panelInferior = new VBox();
        panelSuperior = new VBox();
        panelDerecho = new VBox();
        detalles = new TextArea();
        
        panelInferior.getChildren().addAll(verDibujos,siguienteJugador,salir);
        panelInferior.setAlignment(Pos.CENTER);
        panelInferior.setSpacing(30);
        
        panelDerecho.getChildren().addAll( comboDibujos, detalles);
        panelDerecho.setAlignment(Pos.CENTER);
        panelDerecho.setSpacing(30);
        
        verDibujos.setScaleX(2);
        verDibujos.setScaleX(2);
        siguienteJugador.setScaleX(2);
        siguienteJugador.setScaleX(2);
        salir.setScaleX(2);
        salir.setScaleX(2);
        
        root.setTop(panelSuperior);
        root.setRight(panelDerecho);
        
        root.setBottom(panelInferior);
        
        //Aqui se lee el archivo Json
        leerJson(RUTA);
        
        //en esta parte se grafican los dibujos mediante coordendas
        Canvas canvas = new Canvas(400, 400);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        int numero = (int) (Math.random() * listadoble.size());//Se escoge un dibujo aleatorio
        dibujoActual = (Dibujo) listadoble.get(numero);//se guarda el dibujo en una variable
        drawing(gc,dibujoActual);
        
        //Llenamos las opciones para que el usuario adivine el dibujo
        ObservableList<String> items = FXCollections.observableArrayList();
        llenarComboDibujos(items,dibujoActual);
        comboDibujos.setItems(items);
        root.setCenter(canvas);
        
        comboDibujos.setOnAction(e -> {detalles.setText(comboDibujos.getValue());
                                        verificarRespuesta(dibujoActual,comboDibujos.getValue(),detalles);}) ;

        comboDibujos.valueProperty().addListener((ov, p1, p2) -> {
            System.out.println("Nueva Selección: " + p2);
            System.out.println("Vieja Selección: " + p1);
        });
        verDibujos.setOnAction(el -> {
            try {
                ProyectoEstructura.cambiarVentana(root,new Dibujos().getRoot());
            } catch (IOException | ParseException ex) {
                Logger.getLogger(PaneOrganizer.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        salir.setOnAction(e -> Platform.exit());
        System.out.println("Hay "+  listadoble.size()+" cargados ");
        key_id = ((Dibujo) listadoble.get(numero)).getKey_id();  
        System.out.println(" El key del dibujo actul es: " +key_id);
        System.out.println(" El nombre del dibujo actul es: " +((Dibujo) listadoble.get(numero)).getWord());
        
        
        
        //Estilos a los botones
        root.setStyle("-fx-background-image: url("+FONDO+"); "
                + "-fx-background-position: center center; "
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size:" + Constantes.ANCHO + " " + Constantes.ALTO + ";"
        );
    }
    
    public void leerJson(String ruta) throws FileNotFoundException, IOException, ParseException{
        JSONParser parser = new JSONParser();
        JSONArray a = (JSONArray) parser.parse(new FileReader(ruta));

        for (Object o : a)
          {
            
            
            JSONObject person = (JSONObject) o;
            
            String name = (String) person.get("word");
            String countryCode = (String) person.get("countrycode");
            String timeStamp = (String) person.get("timestamp");
            Boolean recognized = (Boolean) person.get("recognized");
            String key_id = (String) person.get("key_id");
            ArrayList<ArrayList<ArrayList<Double>>> coordenadas = (ArrayList) person.get("drawing");
            
            ArrayList<Trazos> drawing = new ArrayList<>();
            for(ArrayList<ArrayList<Double>> ar:  coordenadas){
                Trazos trazos = new Trazos();
                trazos.setX(ar.get(0));
                trazos.setY(ar.get(1));
                drawing.add(trazos);
            }
            
            Dibujo dibujo = new Dibujo(name,countryCode,timeStamp,recognized,key_id,drawing);
            listadoble.addLast(dibujo);
            
                

          }
    }
    private void drawing(GraphicsContext gc,Dibujo dibujo) {
        
       ArrayList<Trazos> trazos = dibujo.getDrawing();
       ArrayList<Color> colores = new ArrayList<>();
           colores.add(Color.GREEN);
           colores.add(Color.BLUE);
           colores.add(Color.BROWN);
           colores.add(Color.WHITE);
           colores.add(Color.PINK);
           gc.setFill(Color.GREEN);
           gc.setFill(Color.AZURE);
           gc.setFill(Color.CORNSILK);
           gc.setFill(Color.YELLOW);
           gc.setFill(Color.CORAL);
           gc.setFill(Color.DARKORCHID);
           gc.setFill(Color.FORESTGREEN);
       for(Trazos trazo: trazos){
           
           //conversion de JSONArray a Object
           Object[] obx = trazo.getX().toArray();
           Object[] oby = trazo.getY().toArray();
           
           //creando arreglo de double
           double[] coordenadaX = new double[trazo.getX().size()];
           double[] coordenadaY = new double[trazo.getY().size()];
           
           //llenando los arreglos de double[]
           for(int i=0;i<obx.length;i++){
               //El arreglo de objetos devuelve Long
               Long x = (Long)oby[i];
               Long y = (Long)obx[i];
               //Por eso se hace la conversión a double
               double d1 = (double)x;
               double d2 = (double)y;
               //Se llenan los arreglos
               coordenadaX[i] = d1;
               coordenadaY[i] = d2;
           }
           System.out.println("get x:" + trazo.getX());
           System.out.println("get y:" + trazo.getY());
           //Colores diferentes para cada trazo
           gc.setStroke(colores.get((int) (Math.random() * colores.size())));
           gc.setLineWidth(5);
           gc.strokePolygon(coordenadaY,coordenadaX,coordenadaX.length);
           
       }
        /*
        gc.strokeLine(40, 10, 10, 40);
        gc.fillOval(10, 60, 30, 30);
        gc.strokeOval(60, 60, 30, 30);
        gc.fillRoundRect(110, 60, 30, 30, 10, 10);
        gc.strokeRoundRect(160, 60, 30, 30, 10, 10);
        gc.fillArc(10, 110, 30, 30, 45, 240, ArcType.OPEN);
        gc.fillArc(60, 110, 30, 30, 45, 240, ArcType.CHORD);
        gc.fillArc(110, 110, 30, 30, 45, 240, ArcType.ROUND);
        gc.strokeArc(10, 160, 30, 30, 45, 240, ArcType.OPEN);
        gc.strokeArc(60, 160, 30, 30, 45, 240, ArcType.CHORD);
        gc.strokeArc(110, 160, 30, 30, 45, 240, ArcType.ROUND);*/
    }
    
    public void llenarComboDibujos(ObservableList<String> items, Dibujo dibujoActual){
        items.add(dibujoActual.getWord());
        for(int i = 0; i < 5;i++){
            int numero = (int) (Math.random() * listadoble.size());
            Dibujo dibujoTemp = (Dibujo) listadoble.get(numero);
            items.add(dibujoTemp.getWord());
        }
        
    }
    
    public int verificarRespuesta(Dibujo dibujoActual, String respuesta, TextArea detalles){
        int puntaje = 0;
            if(respuesta.equals(dibujoActual.getWord())) {
                detalles.setText("Felicidaes Acertaste! \n" +
                                "El dibujo es:" + dibujoActual.getWord());
                puntaje = 2;
                return puntaje;
            }else{
                detalles.setText("Fallaste! \n" +
                                "El dibujo es :" + dibujoActual.getWord());
                return puntaje;
            }
        
        
    }
    public CircularDoubleLinkedList getListadoble() {
        return listadoble;
    }
    
    
    public BorderPane getRoot(){
        return root;
    }
}


