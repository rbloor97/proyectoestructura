/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoestructura;

public class NodoDoble <E>{
    private E data;
    private NodoDoble <E> previous, next;
    
    public NodoDoble(E data){
        this.data = data;
        this.previous = this.next = null;
    }

    public E getData() {
        return data;
    }

    public void setData(E data) {
        this.data = data;
    }

    public NodoDoble<E> getPrevious() {
        return previous;
    }

    public void setPrevious(NodoDoble<E> previous) {
        this.previous = previous;
    }
    
    public NodoDoble<E> getNext() {
        return next;
    }

    public void setNext(NodoDoble<E> next) {
        this.next = next;
    }

    @Override
    public String toString() {
        return "NodoDoble{" + "data=" + data + '}';
    }
}