/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoestructura;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
//import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Stack;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.stage.Stage;
//import jdk.nashorn.internal.parser.JSONParser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import sun.misc.Queue;
/**
 *
 * @author adan
 */
public class Dibujos {

    private BorderPane root;
    private Button deshacer, rehacer, anterior, siguiente,salir;
    private Pane panel;
    private HBox centro, arriba, abajo;
    
    
    PaneOrganizer listaTrazos;
    
    
    
    circledoublelinkedlist dibujos;
    private String nombre,fecha_hora,codigo,key_id;
    //JSONArray trazos;
    JSONParser parser;
    private boolean reconocido;
    Stack pila,temp, segundo,temporal;
    Nodo actual;
    CircularDoubleLinkedList circular = new CircularDoubleLinkedList();
    Canvas canvas;
    GraphicsContext gc;

    Dibujos() throws IOException, FileNotFoundException, ParseException {
        temporal = new Stack();
        segundo = new Stack();
        pila = new Stack();
        temp = new Stack();
        canvas = new Canvas(400, 400);
        gc = canvas.getGraphicsContext2D();
        centro = new HBox();
        organizarPanel();
    }

    public void organizarPanel() throws IOException, FileNotFoundException, ParseException {
        
        root = new BorderPane();
        listaTrazos = new PaneOrganizer();
        arriba = new HBox();
        abajo = new HBox();
        deshacer = new Button();
        salir = new Button();
        rehacer = new Button();
        anterior = new Button();
        siguiente = new Button();
        panel = new Pane();
        
        //Group root = new Group();
        
        drawShapes();
        //leerdibujos();
        
        
        ImageView imageView0 = new ImageView("imagenes/siguiente.png");
        imageView0.setFitWidth(125);
        imageView0.setFitHeight(125);
        siguiente.setGraphic(imageView0);
        siguiente.setStyle("-fx-background-color:  transparent; ");
        
        
         siguiente.setOnMouseEntered(new EventHandler<MouseEvent>() {
        @Override public void handle(MouseEvent e) {
            siguiente.setScaleX(1.1);
            siguiente.setScaleY(1.1);
            }
        });
 
        siguiente.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override public void handle(MouseEvent e) {
                siguiente.setScaleX(1);
                siguiente.setScaleY(1);
            }
        });
        
        ImageView salir1 = new ImageView("imagenes/salir.png");
        salir1.setFitWidth(100);
        salir1.setFitHeight(100);
        salir.setGraphic(salir1);
        salir.setStyle("-fx-background-color:  transparent; ");
        
         salir.setOnMouseEntered(new EventHandler<MouseEvent>() {
        @Override public void handle(MouseEvent e) {
            salir.setScaleX(1.1);
            salir.setScaleY(1.1);
            }
        });
 
        salir.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override public void handle(MouseEvent e) {
                salir.setScaleX(1);
                salir.setScaleY(1);
            }
        });
        
        
     

        
        ImageView imageView2 = new ImageView("imagenes/atras.png");
        imageView2.setFitWidth(125);
        imageView2.setFitHeight(125);
        anterior.setGraphic(imageView2);
        anterior.setStyle("-fx-background-color:  transparent; ");
       // canvas.setStyle("-fx-background-image: url('/imagenes/fondo.jpg'); ");
       
       anterior.setOnMouseEntered(new EventHandler<MouseEvent>() {
        @Override public void handle(MouseEvent e) {
            anterior.setScaleX(1.1);
            anterior.setScaleY(1.1);
            }
        });
 
        anterior.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override public void handle(MouseEvent e) {
                anterior.setScaleX(1);
                anterior.setScaleY(1);
            }
        });
        
        
        
        ImageView imageView = new ImageView("imagenes/rehacer.png");
        imageView.setFitWidth(125);
        imageView.setFitHeight(150);
        rehacer.setGraphic(imageView);
        rehacer.setStyle("-fx-background-color:  transparent; ");
        
        rehacer.setOnMouseEntered(new EventHandler<MouseEvent>() {
        @Override public void handle(MouseEvent e) {
            rehacer.setScaleX(1.1);
            rehacer.setScaleY(1.1);
            }
        });
 
        rehacer.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override public void handle(MouseEvent e) {
                rehacer.setScaleX(1);
                rehacer.setScaleY(1);
            }
        });
        
        ImageView imageView1 = new ImageView("imagenes/deshacer.png");
        imageView1.setFitWidth(125);
        imageView1.setFitHeight(125);
        deshacer.setGraphic(imageView1);
        deshacer.setStyle("-fx-background-color:  transparent; ");
        
        deshacer.setOnMouseEntered(new EventHandler<MouseEvent>() {
        @Override public void handle(MouseEvent e) {
            deshacer.setScaleX(1.1);
            deshacer.setScaleY(1.1);
            }
        });
 
        deshacer.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override public void handle(MouseEvent e) {
                deshacer.setScaleX(1);
                deshacer.setScaleY(1);
            }
        });
        
        root.setStyle("-fx-background-image: url('/imagenes/fondodibujo.jpg'); "
                + "-fx-background-position: center center; "
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size:" + Constantes.ANCHO + " " + Constantes.ALTO + ";"
        );
        
        abajo.getChildren().addAll(salir,anterior, siguiente);
        arriba.getChildren().addAll(deshacer, rehacer);
        centro.getChildren().addAll(canvas);
        
        root.setTop(arriba);
        root.setBottom(abajo);
        root.setCenter(centro);
        
        root.setCenter(panel);
        root.setCenter(centro);
        
        
        arriba.setAlignment(Pos.CENTER);
        centro.setAlignment(Pos.CENTER);
        arriba.setSpacing(450);
        abajo.setSpacing(245);
        abajo.setAlignment(Pos.TOP_LEFT);
        centro.setAlignment(Pos.CENTER);
        
        
        
        ImageView centro1 = new ImageView("imagenes/hacer.png");
        centro1.setFitWidth(200);
        imageView.setFitHeight(100);
        //centro.getChildren().addAll(centro1);

        
        
        
        
        siguiente.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(ActionEvent t) {
                centro.getChildren().remove(canvas);
                System.out.println("a");
                actual = actual.getNext();
              //  System.out.println(actual.toString());
                cargar();
                System.out.println(actual);
                
        }});
        
        anterior.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(ActionEvent t) {
                System.out.println("a");
                actual = actual.getPrevious();
                cargar();
                System.out.println(actual);
                
        }});
        
          deshacer.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(ActionEvent t) {
                System.out.println("a");
                
               // actual = actual.getNext();
                dibujardeshacer();
                System.out.println(actual);
                
        }});
          
          rehacer.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(ActionEvent t) {
                System.out.println("a");
                
               // actual = actual.getNext();
                dibujarrehacer();
                System.out.println(actual);
                
        }});
 
        
    }
    
    
    public void dibujarprimero(){
        
    }
    
    public void dibujarrehacer(){
        if(!temporal.isEmpty()){
            Stack n = new Stack();
            centro.getChildren().remove(canvas);
            canvas = new Canvas(400, 400);
            
            GraphicsContext gc = canvas.getGraphicsContext2D();
            gc.setFill(Color.WHITE);
            gc.setStroke(Color.AQUA);
            //gc.setFill(Color.GREEN);
            //gc.setStroke(Color.BLUE);
            gc.setLineWidth(5);
            
            temp.push(temporal.pop());
            temporal.size();
            temp.size();
          //  gc.strokeLine(40, 10, 10, 40);
            while(!temp.isEmpty()){
               // temporal.push(temp.peek());
               n.push(temp.peek());
                Trazo xy = (Trazo) temp.pop();
                gc.strokePolyline(xy.x, xy.y, xy.x.length);
            }
            centro.getChildren().addAll(canvas);
            temporal.size();
            System.out.println(centro.getChildren().size());
            while(!n.isEmpty()){
                temp.push(n.pop());
            }
        }
        
        else{
            
            System.out.println("listavacia");
        }

        
        
        
        
    }
    
    

    BorderPane getRoot() {
        return root;
    }
    
    
    
    public void dibujardeshacer(){
        if(!temp.isEmpty()){
            Stack n = new Stack();
            centro.getChildren().remove(canvas);
            canvas = new Canvas(400, 400);
            GraphicsContext gc = canvas.getGraphicsContext2D();
            gc.setFill(Color.CORAL);
            gc.setStroke(Color.MISTYROSE);
            gc.setLineWidth(5);
            temporal.push(temp.pop());
            temporal.size();
            temp.size();
          //  gc.strokeLine(40, 10, 10, 40);
            while(!temp.isEmpty()){
               // temporal.push(temp.peek());
               n.push(temp.peek());
                Trazo xy = (Trazo) temp.pop();
                gc.strokePolyline(xy.x, xy.y, xy.x.length);
            }
            centro.getChildren().addAll(canvas);
            temporal.size();
            System.out.println(centro.getChildren().size());
            while(!n.isEmpty()){
                temp.push(n.pop());
            }
        }
        
        else{
            centro.getChildren().remove(canvas);
            canvas = new Canvas(400, 400);
            GraphicsContext gc = canvas.getGraphicsContext2D();
            Image image = new Image("imagenes/nohay.png");

            gc.drawImage(image, 75, 75, 250, 250);

           // gc.drawImage(image, 220, 50, 100, 70);
            centro.getChildren().addAll(canvas);

            
            System.out.println("listavacia");
        }

    }
    public void cargar(){
        
        centro.getChildren().remove(canvas);
        
        System.out.println("");
        System.out.println(centro.getChildren().size());
                
        canvas = new Canvas(400, 400);
        
        while(!temp.isEmpty()){
            temp.pop();
        }
        
        while(!temporal.isEmpty()){
            temporal.pop();
        }
        
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.GREEN);
        gc.setStroke(Color.BLUE);
        gc.setLineWidth(5);
      //  gc.strokeLine(40, 10, 10, 40);
        Dibujo dibujoActual = (Dibujo)actual.getData();
        
        
        for (Trazos t: dibujoActual.getDrawing()) {
            segundo.push(t);   
            temp.push(t);
        }
        
        while(!segundo.isEmpty()){
            
            Trazos xy = (Trazos) segundo.pop();
            Integer[] arx = (Integer[])xy.getX().toArray(new Integer[0]);
            Integer[] ary = (Integer[])xy.getY().toArray(new Integer[0]);
            
            double[] x = new double[arx.length];
            double[] y = new double[arx.length];
                for (int i = 0; i < arx.length; i++) {
                  x[i] = arx[i].doubleValue();
                  y[i] = ary[i].doubleValue();
                  gc.strokePolyline(x, y, x.length);
                }

           // final double[] result = new double[arx.length];
           
          /*  for(int i=0;i<arx.length;i++){
                double a = arx[i].doubleValue();
                double[] n = ary[i].doubleValue();
                gc.strokePolyline(a, n, 10);
               }*/
            
            
        }

        centro.getChildren().addAll(canvas);
        System.out.println(centro.getChildren().size());
    }
    
    
    public Map Leerjason() throws FileNotFoundException,IOException, ParseException {
 
        parser = new JSONParser();
        JSONArray jsonArray = (JSONArray) parser.parse(new FileReader("C:/Users/user/Documents/NetBeansProjects/Proyecto_estructuras_datos/src/proyecto_estructuras_datos/dataset.ndjson"));

        for (Object o : jsonArray) {
            JSONObject person = (JSONObject) o;

             nombre = (String) person.get("word");
            //System.out.println("Word::::" + nombre);

            codigo = (String) person.get("countrycode");
            //System.out.println("Countrycode::::" + codigo);
            
            fecha_hora  = (String) person.get("timestamp");
            //System.out.println("Timestamp::::" + fecha_hora);
            
            reconocido = (boolean) person.get("recognized");
            //System.out.println("Recognized::::" + reconocido);
            
            key_id = (String) person.get("key_id");
            //System.out.println("Key_id::::" + key_id);
            
            //JSONArray drawing = (JSONArray) person.get("drawing");
           /*  trazos = (JSONArray) person.get("drawing");

            for (Object object : trazos) {
               // System.out.println("Drawing::::" + drawing);
                
            }
            //System.out.println("Drawing::::" + trazos);*/
}       return null;
}
    
    private void drawShapes() {
        gc.setFill(Color.GREEN);
        gc.setStroke(Color.BLUE);
        gc.setLineWidth(5);
        
        double[] n = {150,31,54,82,99,119,136,140,138,129,112,99,69,39,29};
        double[] a = {217,239,253,254,138,129,112,99};
        
         double[] n1 = {50,31,54,82,99,119,136,140,138,129,112,99,69,39,29};
        double[] a1 = {217,239,253,5,138,129,112,99};
        
        
        
        double[] a2 = {150,239,253,10,9,136,1};
        double[] a0 = {2,10,253,0,9,136,1};
        
        
       /* Trazo trazo = new Trazo(a,a1);
        Trazo trazo1 = new Trazo(n1,n);
        ArrayList<Trazo> trazos = new ArrayList<>();
        trazos.add(trazo);
        trazos.add(trazo1);
        Dibujo dibujo = new Dibujo(trazos);
        
        
        Trazo trazo2 = new Trazo(a2,a0);
        Trazo trazo3 = new Trazo(n1,n);
        ArrayList<Trazo> trazos1 = new ArrayList<>();
        trazos1.add(trazo2);
       // trazos1.add(trazo3);
        Dibujo dibujo1 = new Dibujo(trazos1);
        ArrayList<double[]> xy = new ArrayList<double[]>();
        
        
        xy.add(n);
        xy.add(a);
 
       double[] n2 = {0,31,54,82,99,119,136,140,10,129,112,99,69,39,29};
        double[] a5 = {54,82,99,30};
        
        double[] a6 = {0,10,99,30};
        Trazo trazo4 = new Trazo(a5,a5);
        Trazo trazo5 = new Trazo(a6,a5);
        ArrayList<Trazo> trazos2 = new ArrayList<>();
        trazos2.add(trazo4);
        trazos2.add(trazo5);
        Dibujo dibujo2 = new Dibujo(trazos2);
        
        circular.addLast(dibujo);
        circular.addLast(dibujo1);
        circular.addLast(dibujo2);*/
        
        actual = listaTrazos.getListadoble().getNodo();

        Dibujo dibujoActual = (Dibujo)actual.getData();
        
        for (Trazos t: dibujoActual.getDrawing()) {
            temp.push(t);
            segundo.push(t);
           // gc.strokePolyline(t.x, t.y, t.x.length);
        }
         while(!segundo.isEmpty()){
             
             Trazos xy = (Trazos) segundo.pop();
             
             Object[] obx = xy.getX().toArray();
             Object[] oby = xy.getY().toArray();
             
//            Double[] arx = new Double[xy.getX().size()];
//            arx = xy.getX().toArray(arx);
//            Double[] ary = new Double[xy.getY().size()];
//            ary = xy.getX().toArray(ary);
            
//            Double[] arx = (Double[])xy.getX().toArray(new Double[0]);
//            Double[] ary = (Double[])xy.getY().toArray(new Double[0]);
//            
            double[] x = new double[xy.getX().size()];
            double[] y = new double[xy.getY().size()];
                for (int i = 0; i < obx.length; i++) {
                    //El arreglo de objetos devuelve Long
               Long lx = (Long)oby[i];
               Long ly = (Long)obx[i];
               //Por eso se hace la conversión a double
               double d1 = (double)lx;
               double d2 = (double)ly;
                  x[i] = d1;
                  y[i] = d2;
                  
                }
                gc.strokePolyline(x, y, x.length);
            
           /* Trazo m = (Trazo) segundo.pop();
            gc.strokePolyline(m.x, m.y, m.x.length);*/
            
        }
        //cargar();

    }
    
    
    
    public void deshacer(){
        if(!temp.isEmpty()){
            Stack temporal = new Stack();
            temporal.push(temp.pop());
        
       /* if(!pila.isEmpty()){
        temp.push(pila.pop());
        }
        while(!pila.isEmpty()){
            double [] m1 = (double[]) pila.pop();
            double [] m2 = (double[]) pila.pop();}
        System.out.println(pila.peek());*/
        //cargardibujo();
        cargar();
        }
        else{
            System.out.println("vacia");
        }
        
        
    }
    public void rehacer(){
        if(!temp.isEmpty()){
            pila.push(temp.pop());
        }
        
    
    }
}

